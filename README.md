# README #

### Cupcake Entertainment - Jokenpo ###

* Repo responsável pelo versionamento do projeto de um jogo de Jokenpo para o processo seletivo da empresa Cupcake Entertainment
* 0.1

### Instalação ###
Uma vez que você baixou o conteudo do repositorio coloque os arquivos dentro de uma pasta no seu servidor apache (pasta Htdocs se você utiliza um servidor XAMPP ou a WWW em outros casos.)
Inicie o servidor Apache e acesse o jogo atravez do Browser(localhost/SUA_PASTA), Google Chrome de preferência. A partir desse ponto é só jogar e se divertir.

### Linguagem ###

Projeto desenvolvido em JS utilizando classes.
A Pasta Framework contem todas as classes bases que podem ser reutilizadas em projetos futuros. A ideia é que essas classes sejam o mais abrangentes possíveis; contendo métodos de input, física, render, áudio, etc...
Já os Scripts que estiverem fora dessa pastas são Classes exclusivas do jogo em desenvolvimento. 

### Ricardo Bugan ###