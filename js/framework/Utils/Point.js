var Point = Class.extend({
    x : 0,
    y : 0,
    z : 0,
    
    init : function(x, y, z){
        this.x = x || 0;
        this.y = y || 0;
        this.z = z || 0;
    },
    sum : function(x, y, z){
        var _x = x || 0;
        var _y = y || 0;
        var _z = z || 0;
        return new Point(this.x + _x ,this.y + _y, this.z + _z);
    },
    sub : function(x, y, z){
        var _x = x || 0;
        var _y = y || 0;
        var _z = z || 0;
        return new Point(this.x - _x ,this.y - _y, this.z - _z);
    }
});

Point.between = function(pointA , pointB){
    return new Point( (pointA.x + pointB.x) / 2, (pointA.y + pointB.y) / 2, (pointA.z + pointB.z) / 2);
}