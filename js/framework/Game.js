var Game = Class.extend({
    canvas : null,
    context : null,
    lastTime: 0,
    elapsedTime :0,
    mouse : null,

    init : function(canvas){
        this.canvas = canvas;
        this.context = this.canvas.getContext("2d");
        this.lastTime = new Date().getTime();
        this.mouse =  new MouseInput(this.canvas);
    },

    loop : function(){
        this.update();
        this.draw();
    },

    draw : function(){
        this.context.clearRect(0,0, this.canvas.width, this.canvas.height);
        if(SceneManager.current != null)
            SceneManager.current.draw(this.context);
    },

    update : function(){
        //atualizando tempo do jogo
        var date = new Date().getTime();
        this.elapsedTime = date - this.lastTime;
        this.lastTime = date;
        if(SceneManager.current != null)
            SceneManager.current.update(this.elapsedTime);
    }

});