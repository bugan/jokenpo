var Hitbox = Class.extend({
    position : null, 
    width : null,
    height : null,
    name : null,

    init : function(pos, width, height){
        this.position = pos;
        this.width = width;
        this.height = height;
    },
    testHitPoint : function(point){
        if(this.position.x <= point.x && (this.position.x+this.width) >= point.x)
            if(this.position.y <= point.y && (this.position.y+this.height) >= point.y)
                return true;
        return false;   
    }
});