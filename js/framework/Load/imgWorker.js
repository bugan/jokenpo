running = false;
requests = [];
self.onmessage = function(message){
        processIMG(message.data);
}

function processIMG(byteArray){
    var byteArray = new Uint8Array(byteArray);
    var len = byteArray.byteLength;
    var binary = '';
    for (var i = 0; i < len; i++) {
        binary += String.fromCharCode(byteArray[i]);
    }
    var url = 'data:image/jpeg;base64,' + btoa(binary);
    self.postMessage({url : url});

}