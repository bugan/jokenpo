FileLoader = function(){}
FileLoader.loadedImages = {};
FileLoader.loadedJSON = {};

FileLoader.fetchArray = function(name, arrayBuffer, callback){
    if(typeof(Worker) !== "undefined") {
        // Web worker support
        var worker = new Worker('js/framework/Load/imgWorker.js');

        worker.postMessage(arrayBuffer);
        worker.onmessage = function(message){
            FileLoader.loadedImages[name] = message.data.url;
            callback();
            message.currentTarget.terminate();
            worker.terminate();
            worker = null;
        }
    } else {
        // Sorry! No Web Worker support..
        var byteArray = new Uint8Array(arrayBuffer);
        var len = byteArray.byteLength;
        var binary = '';
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(byteArray[i]);
        }
        var url = 'data:image/jpeg;base64,' + btoa(binary);
        FileLoader.loadedImages[name] = url;
        callback();

        url = null;
        byteArray = null;
        len = null;
        binary = null;
    }
}

FileLoader.loadBatch = function(names, srcs, onload){
    var remainingFiles = names.length;
    for(var i=0 ; i<names.length ; i++){
        if(srcs[i].match(/.png/g)){
            FileLoader.loadImage(names[i], srcs[i], function(){
                remainingFiles--;
                if(remainingFiles <= 0){
                    FileLoader.loaded(onload);

                }
            });    
        }else if(srcs[i].match(/.json/g)){
            FileLoader.loadJson(names[i], srcs[i], function(){
                remainingFiles--;
                if(remainingFiles <= 0){
                    FileLoader.loaded(onload);
                }
            });
        }
    }
}

FileLoader.loaded =  function(callback)
{
    for(var src in FileLoader.loadedImages){
        var img =  new Image();
        img.src = FileLoader.loadedImages[src];
        FileLoader.loadedImages[src] = img;
        img = null;
    }
    remainingFiles = null;
    callback();
}

FileLoader.loadImage = function(name, src, onload){
    FileLoader.sendRequest(name, src, FileLoader.fetchArray, onload);
}

FileLoader.sendRequest = function(name, src, callback, onload){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", src, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function(){
        callback(name, xhr.response, onload);
        xhr = null;
    }
    xhr.send(null);
}
FileLoader.loadJson = function(name, src, callback){
    var xhr = new XMLHttpRequest();
    xhr.open("GET", src, true);
    xhr.responseType = 'json';
    xhr.onload = function(){
        FileLoader.loadedJSON[name] = xhr.response;
        callback();
        xhr = null;
    }
    xhr.send(null);
}