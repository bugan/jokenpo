var Scene = Class.extend({
    gameObjects : null,

    init : function()
    {
        this.gameObjects =  new Array();
    },

    addGameObject : function(gameObject)
    {

        this.gameObjects.push(gameObject);
    },

    draw : function(context)
    {
        for(var i=0 ; i< this.gameObjects.length ; i++){
            this.gameObjects[i].draw(context);    
        }
    },

    update : function(elapsedTime)
    {
        for(var i=0 ; i< this.gameObjects.length ; i++){
            this.gameObjects[i].update(elapsedTime);    
        }
    },

    clickEvent : function(point)
    {
        var child;
        for(var i=0 ; i< this.gameObjects.length ; i++)
        {
            child = this.gameObjects[i];

            if(child.hitbox != null)
                if(child.hitbox.testHitPoint(point))
                    child.onClick();
        }
    },

    mouseMoveEvent : function(point)
    {
        var child;
        for(var i=0 ; i< this.gameObjects.length ; i++)
        {
            child = this.gameObjects[i];
            if(child instanceof Button && child.hitbox != null)
                if(child.hitbox.testHitPoint(point))
                    child.onMouseOver();
                else
                    child.onMouseOut();
        }
    },

    onEnable : function()
    {
        
    },
    
    onDisable : function()
    {
        
    }

});