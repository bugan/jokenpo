SceneManager = function(){};
SceneManager.scenes = {};
SceneManager.current = null;

SceneManager.addScene = function(name, scene){
    if(!(scene instanceof Scene)){
        console.warn("SceneManager Error! Trying to add an obj other than an Scene");
        return;
    }
    
    SceneManager.scenes[name] = scene;
    
    if(SceneManager.current == null)
        SceneManager.current = scene;
}

SceneManager.gotoScene = function(name){
    if(SceneManager.scenes[name] == null || SceneManager.scenes[name] == undefined){
        console.warn("SceneManager Error! Scene not found");
        return;
    }
    if(SceneManager.current != null)
        SceneManager.current.onDisable();
    
    SceneManager.current = SceneManager.scenes[name];
    SceneManager.current.onEnable();
}