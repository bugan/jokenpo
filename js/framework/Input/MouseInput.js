var MouseInput = Class.extend({
    init : function(canvas){
        var _self = this;

        canvas.addEventListener(
            "click",
            function(event){
                event.stopPropagation();
                event.preventDefault();

                var point = new Point();
                point.x = event.offsetX;
                point.y = event.offsetY;
                _self.clickEvent(point);
            }, 
            true);

        canvas.addEventListener(
            "mousemove",
            function(event){
                event.stopPropagation();
                event.preventDefault();

                var point = new Point();
                point.x = event.offsetX;
                point.y = event.offsetY;
                _self.mouseMoveEvent(point);
            }, 
            true);
    },

    clickEvent : function(point){
        SceneManager.current.clickEvent(point);
    },

    mouseMoveEvent : function(point){
        if(SceneManager.current != null)    
            SceneManager.current.mouseMoveEvent(point);
    }
});