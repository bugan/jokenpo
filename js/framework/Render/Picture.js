var Picture = Class.extend({
    _imageRef : null, 
    _crop : null, 
    name : null, 
    scale : null,
    position : null,
    
    init : function(name, info, pattern){
        this.name = name;
        this._imageRef = info;
        this._crop = null;
        this.position = new Point();
        this.scale = new Point(1,1);
        
        if(pattern != null){
            var info = FileLoader.loadedJSON[info];
            var frame;
            for(var i=0 ; i<info.frames.length ; i++){
                frame = info.frames[i];
                if(frame.filename.match(pattern)){
                    this._crop = frame.frame;
                    break;
                }
            }
        }
       
    }, 
    
    draw : function(context){
        if(this._crop != null){
              context.drawImage(FileLoader.loadedImages[this._imageRef], this._crop.x, this._crop.y, this._crop.w, this._crop.h, this.position.x, this.position.y, this._crop.w * this.scale.x, this._crop.h*this.scale.y);
        }else{
            context.drawImage(FileLoader.loadedImages[this._imageRef], this.position.x, this.position.y);
        }
    },
    
    moveTo : function(x , y){
        this.position.x = x;    
        this.position.y = y;
    },
    
    move : function(x, y){
        this.position.x += x;    
        this.position.y += y;    
    }
});