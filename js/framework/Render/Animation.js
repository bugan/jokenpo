var Animation = Class.extend({
    _imageRef : null,
    _lastTime : 0,
    _frames : [],
    _frameScript : [],
    _behaviorCalled : null,
    loop : false,
    playing : false,
    currentFrame : 0,
    name : null,
    numFrames : 0,
    fps: 24,
    scale : null, 

    init : function(name, pattern,  info){
        this.name = name;
        this._imageRef = info;
        this._lastTime = 0;
        this._frames = [];
        this._frameScript = [];
        this.loop = true;
        this.playing = true;
        this.currentFrame = 0;
        this.numFrames = 0;
        this.fps = 24;
        this.scale = new Point(1,1);

        var info = FileLoader.loadedJSON[info];
        var frame;
        var frameInfo;
        for(var i=0 ; i<info.frames.length ; i++){
            frame = info.frames[i];
            if(frame.filename.match(pattern)){
                frameInfo = frame.frame;
                if(info.frames[i].trimmed){
                    frameInfo.offsetX = frame.spriteSourceSize.x - (frame.pivot.x * frame.sourceSize.w);
                    frameInfo.offsetY = frame.spriteSourceSize.y - (frame.pivot.y * frame.sourceSize.h);    
                }else{
                    frameInfo.offsetX = 0;
                    frameInfo.offsetY = 0;
                }

                this._frames.push(frameInfo);
                this.numFrames++;
            }
        }
    },
    addFrameScript : function(frame, behavior){
        this._frameScript[frame] = behavior;
    },

    getAnimationFrame : function(){
        if(!this._behaviorCalled && this._frameScript[this.currentFrame] != null){
            this._frameScript[this.currentFrame].call();
            this._behaviorCalled = true;
        }
        return [this._imageRef,this._frames[this.currentFrame]];
    },

    play : function(){
        var _self = this;
        if(!this.playing){
            this._lastTime = 0;
            this.playing = true;
        }
    },

    pause : function(){
        this.playing = false;
    },

    stop : function(){
        this.playing = false;
        this.currentFrame = 0;
    },

    update : function(elapsedTime){
        if(this.playing){
            var passFrames = 0;
            var time = elapsedTime + this._lastTime;
            this._lastTime += elapsedTime;
            if(time >= 1000/this.fps){
                passFrames = parseInt(time/(1000/this.fps));
                this._lastTime = 0;
                this.currentFrame += passFrames;
                this._behaviorCalled = false;
            }

            if(this.currentFrame >= this.numFrames){
                if(!this.loop){
                    this.pause();
                    this.currentFrame = this.numFrames - 1;
                }else{
                    this.currentFrame = 0;
                }
            }
            passFrames = null;
        }
    },
});