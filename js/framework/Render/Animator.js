var Animator = Class.extend({
    currentState : null,
    states : null,
    position : null, 
    visible : null,

    init : function(){
        this.currentState = null;
        this.states = {};
        this.position =  new Point();
        this.visible = true;
    },
    addState : function(animation){
        this.states[animation.name] = animation;

        if(this.currentState == null)
            this.currentState = animation;
    },
    changeState : function(name){
        if(this.states[name] != null && this.currentState.name != name)
        {
            this.currentState.stop();
            this.currentState = this.states[name];
            this.currentState.play();
        }
    },
    update : function(elapsedTime){
        this.currentState.update(elapsedTime);
    },
    draw : function(context){
        var info = this.currentState.getAnimationFrame();
        var frameInfo = info[1];
        context.drawImage(FileLoader.loadedImages[info[0]], frameInfo.x, frameInfo.y, frameInfo.w, frameInfo.h, this.position.x+frameInfo.offsetX, this.position.y+frameInfo.offsetY, frameInfo.w * this.currentState.scale.x, frameInfo.h * this.currentState.scale.y);
        //console.log(frameInfo.offsetX);

        info = null;
        frameInfo = null;
    }
});
