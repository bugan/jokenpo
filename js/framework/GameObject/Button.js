var Button = GameObject.extend({
    callback : null,
    
    
    init : function(x, y, info, idlePattern, overPattern, selectPattern){
        var _self = this;
        this._super(x, y);
       
        
        this.animator =  new Animator();
        
        var anim = new Animation("idle", idlePattern, info);
        this.animator.addState(anim);
        
        anim = new Animation("over", overPattern, info);
        anim.loop = false;
        this.animator.addState(anim);
        
        anim = new Animation("select", selectPattern, info);
        anim.loop = false;
    
        this.animator.addState(anim);
        
        var frameinfo  = this.animator.currentState.getAnimationFrame()[1];
        this.hitbox = new Hitbox(this.position, frameinfo.w, frameinfo.h);
        
        frameinfo = null;
        anim = null;
        
    },
    
    draw : function(context){
       this.animator.draw(context);
    },

    update : function(elapsedTime){
        this.animator.position = this.position;
        this.animator.update(elapsedTime);
    },
    
    onClick : function(){
        this._callback();
        this.animator.changeState("select");
    },
    
    onMouseOver : function(){
        this.animator.changeState("over");
    },
    
    onMouseOut : function(){
        this.animator.changeState("idle");
    },
    
    _callback : function()
    {
        if(this.callback != undefined && this.callback != null)
            this.callback();
    }
    
});