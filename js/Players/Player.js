var Player = GameObject.extend({
    choosed: null,
    score : null,
    name: "",
    images : null,
    last : null, 

    init : function(x, y){
        this._super(x, y);
        this.score = 0;
        this.images =  new Array();

        var pic = new Picture("pedraImg", "hud", "pedra_idle");
        pic.position = this.position;
        this.images.push(pic);

        pic = new Picture("pedraImg", "hud", "tesoura_idle");
        pic.position = this.position;
        this.images.push(pic);

        pic = new Picture("pedraImg", "hud", "papel_idle");
        pic.position = this.position;
        this.images.push(pic);

    },

    draw : function(context)
    {
        this._super(context);
        if(this.last != null && this.last != -1)
        {
            this.images[this.last].position = this.position;
            this.images[this.last].draw(context);
        }
    },

    reset : function()
    {
        if(this.choosed != null)
            this.last = this.choosed.index;
        else
            this.last = null;
        
        this.choosed = null;
    }
});