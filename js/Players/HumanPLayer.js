var HumanPlayer = Player.extend({
    rockBtn : null,
    paperBtn : null,
    scissorsBtn : null,
    btnOffsetY : 200,
    init : function(x, y){
        this._super(x, y);
        this.createButtons();
    },

    createButtons : function()
    {
        var _self = this;
        //botão para selecionar pedra
        this.rockBtn = new Button(this.position.x-50, this.position.y+this.btnOffsetY,"hud",
                                  "pedra_idle",
                                  "pedra_over",
                                  "pedra_select");
        this.rockBtn.callback = function()
        {
           
            _self.choosed = choices.rock;
        }

        //botão para selecionar papel
        this.paperBtn = new Button(this.position.x+150, this.position.y+this.btnOffsetY,"hud",
                                   "papel_idle",
                                   "papel_over",
                                   "papel_select");
        this.paperBtn.callback = function()
        {
           
            _self.choosed = choices.paper;
        }

        //botão para selecionar tesoura
        this.scissorsBtn = new Button(this.position.x+350, this.position.y+this.btnOffsetY,"hud",
                                      "tesoura_idle",
                                      "tesoura_over",
                                      "tesoura_select");
        this.scissorsBtn.callback = function()
        {
            _self.choosed = choices.scissors;
        }

    },

    update: function(elapsedTime)
    {

        this.rockBtn.update(elapsedTime);      
        this.paperBtn.update(elapsedTime);      
        this.scissorsBtn.update(elapsedTime);      
    },
    draw : function(context)
    {
        this._super(context);
        this.rockBtn.draw(context);      
        this.paperBtn.draw(context);      
        this.scissorsBtn.draw(context);
    },

    onClick : function(point){
        if(this.rockBtn.hitbox.testHitPoint(point))
            this.rockBtn.onClick();
        
        if(this.paperBtn.hitbox.testHitPoint(point))
            this.paperBtn.onClick();
        
        if(this.scissorsBtn.hitbox.testHitPoint(point))
            this.scissorsBtn.onClick();


    },

    mouseMoveEvent : function(point){
        if(this.rockBtn.hitbox.testHitPoint(point))
            this.rockBtn.onMouseOver();
        else
            this.rockBtn.onMouseOut();

        if(this.scissorsBtn.hitbox.testHitPoint(point))
            this.scissorsBtn.onMouseOver();
        else
            this.scissorsBtn.onMouseOut();

        if(this.paperBtn.hitbox.testHitPoint(point))
            this.paperBtn.onMouseOver();
        else
            this.paperBtn.onMouseOut();
    },


});