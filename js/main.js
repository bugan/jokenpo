var game;
var choices = {
    rock : new Rock(),
    scissors : new Scissors(),
    paper : new Paper()
}

function init(){
    var canvas = document.getElementById("canvas-game");
    var context = canvas.getContext("2d");

    displayLoading(canvas, context);

    game = new Game(canvas);

    names = [
        "vitoria", "vitoria",
        "hud", "hud"
    ];

    srcs = [
        "assets/animations/trofeu_vitoria.png", "assets/animations/trofeu_vitoria.json",
        "assets/animations/hudAtlas.png", "assets/animations/hudAtlas.json",
    ];

    FileLoader.loadBatch(names, srcs, function(){
        console.log("all images loaded");


        var menu =  new Menu();
        var menuFinal =  new MenuFinal();
        var jokenpo =  new Jokenpo();

        SceneManager.addScene("menu", menu);
        SceneManager.addScene("final", menuFinal);
        SceneManager.addScene("jokenpo", jokenpo);

        loop();
    });

    canvas = null;
    context = null;
}

function loop(){
    game.loop();
    requestAnimationFrame(loop);
}

function displayLoading(canvas, context){
    context.font="30px Verdana";
    context.fillText("Loading", canvas.width * .85, canvas.height * .95);
}