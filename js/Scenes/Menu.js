var Menu = Scene.extend({
    playerBtn : null,
    pcBtn : null, 


    init : function()
    {
        this._super();
        this.createButons();
    },

    createButons :function()
    {
        playerBtn = new Button(500,200, "hud", "botao_player_pc_idle" , "botao_player_pc_over", "botao_player_pc_select");
        playerBtn.callback = function()
        {
            SceneManager.scenes["jokenpo"].playerGame = true;
            SceneManager.gotoScene("jokenpo");
        }
        
        pcBtn = new Button(500,400, "hud", "botao_pc_pc_idle" , "botao_pc_pc_over", "botao_pc_pc_select");
        pcBtn.callback = function()
        {
            SceneManager.scenes["jokenpo"].playerGame = false;
            SceneManager.gotoScene("jokenpo");
        }
        this.addGameObject(playerBtn);
        this.addGameObject(pcBtn);
    },

  
});