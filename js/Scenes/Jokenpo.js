var Jokenpo = Scene.extend({
    playerTime : 3500,
    pcTime : 2000,
    pointsToWin : 3, 
    playerGame : true,
    p1 : null,
    p2 : null,
    currentTimer : null,
    totalTime : null, 
    verified : null,
    canClick : true,
    draw : false,

    init : function()
    {
        this._super();

    },

    onEnable : function()
    {
        if(this.playerGame)
        {
            //criar jogo pc X player
            this.p1 =  new HumanPlayer(250, 200);
            this.p1.name = "Jogador";
            this.p2 =  new PcPlayer(650 , 200);
            this.p2.name = "PC1";

            this.currentTimer = this.totalTime = this.playerTime;
        }else{
            //Criar jogo de pc X pc
            this.p1 =  new PcPlayer(250 , 200);
            this.p1.name = "PC1";
            this.p2 =  new PcPlayer(650 , 200);
            this.p2.name = "PC2";

            this.currentTimer = this.totalTime = this.pcTime;
        }

        this.gameObjects = new Array();
        this.addGameObject(this.p1);
        this.addGameObject(this.p2);

        this.canClick = true;
        this.verified = false;

    },

    update : function(elapsedTime)
    {
        this._super(elapsedTime);
        this.currentTimer -= elapsedTime;

        if(this.currentTimer < 0)
        {
            this.canClick = false;

            //momento em que o pc faz a escolha dele
            if(this.p1 instanceof PcPlayer)
                this.p1.choose();
            if(this.p2 instanceof PcPlayer)
                this.p2.choose();

            if(!this.verified)
                this.verifyVictory();
        }
    },
    draw : function(context)
    {
        this._super(context);

        //desenhando Barra de tempo
        context.fillText( "Tempo: ", 10, 35);
        context.fillRect(130,10, 850 * Math.max((this.currentTimer / this.totalTime),0) , 30);

        //pontuação
        context.fillText(this.p1.name +": " + this.p1.score, 100, 370);
        context.fillText(this.p2.name +": " + this.p2.score, 750, 370);

    },

    clickEvent : function(point)
    {
        if(!this.canClick)
            return;

        this._super(point);

        //Passando evento de Click para o Player verificar seus botões...
        // dessa forma conseguiremos criar um jogo player x player se quisermos
        if(this.p1 instanceof HumanPlayer)
            this.p1.onClick(point);

        if(this.p2 instanceof HumanPlayer)
            this.p2.onclick(point);

    },
    mouseMoveEvent : function(point)
    {
        if(!this.canClick)
            return;

        this._super(point);

        if(this.p1 instanceof HumanPlayer)
            this.p1.mouseMoveEvent(point);

        if(this.p2 instanceof HumanPlayer)
            this.p2.mouseMoveEvent(point);
    },

    verifyVictory : function()
    {
        this.verified = true;
        //verificação de empate
        if(this.p1.choosed == this.p2.choosed)
        {
            this.reset();
            return;
        }
         //Caso algum jogador tenha deixado de escolher o que irá jogar.
        if(this.p1.choosed == null )
        {
            this.winRound(this.p2);
            return;

        }

        if(this.p2.choosed == null)
        {
            this.winRound(this.p1);
            return;
        }
        
        //verificando resultado da rodada, caso os dois jogadores tenham
        //opções diferentes
        if(this.p1.choosed.winsAgainst(this.p2.choosed))
             this.winRound(this.p1);
        else 
            this.winRound(this.p2);
            
    },

    winRound : function(player)
    {
        //Cada partida é uma melhor de 5
        // Assim se algum jogador fizer 3 pontos ele ganha automaticamente
        player.score++;
        if(player.score >= this.pointsToWin)
        {
            SceneManager.scenes["final"].winner = player;
            SceneManager.gotoScene("final");
        }
        else
            this.reset();
    },
    reset : function()
    {   
        //Iniciando uma nova rodada
        //reiniciando Timer
        this.currentTimer = this.totalTime;
        
        //Reiniciando jogadores
        this.p1.reset();
        this.p2.reset();

        //liberando eventos de mouse
        this.canClick = true;
        //liberando verificação uma vez que o timer acabar.
        this.verified = false;
    }
})