var MenuFinal = Scene.extend({
    trof_animator : null,
    winner : null,
    
    init : function()
    {
        this._super();
        this.createVictoryAnim();
        this.createButtons();
    },
    createVictoryAnim : function()
    {
        var anim = new Animation("vitoria", "trofeu_vitoria", "vitoria");
        anim.loop = false;
        
        this.trof_animator =  new Animator();
        this.trof_animator.position.x = 500;
        this.trof_animator.position.y = 225;
        this.trof_animator.addState(anim);
        this.addGameObject(this.trof_animator);
    },
    
    createButtons : function()
    {
        //botao de voltar ao menu
        var button = new Button(100,450, "hud", "voltar_menu_idle" , "voltar_menu_over", "voltar_menu_idle");
        button.callback = function()
        {
            SceneManager.gotoScene("menu");
        }
        this.addGameObject(button);
        
        //botao de jogar novamente
        var button = new Button(750,450, "hud", "jogar_novamente_idle" , "jogar_novamente_over", "jogar_novamente_idle");
        button.callback = function()
        {
            SceneManager.gotoScene("jokenpo");
        }
        this.addGameObject(button);
    },
    draw : function(context)
    {
        this._super(context);
        
        context.fillText("The Winner is: " + this.winner.name,350,400);
    },
    
    onEnable : function()
    {
        this.trof_animator.currentState.play();
    },
    
    onDisable : function()
    {
        this.trof_animator.currentState.stop();
    }
});